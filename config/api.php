<?php

return [
    'weatherbit' => [
        'client' => [
            'base_uri' => env('WEATHERBIT_API_BASE_URI'),
        ],
        'settings' => [
            'api_key' => env('WEATHERBIT_API_KEY'),
        ],
    ],
];
