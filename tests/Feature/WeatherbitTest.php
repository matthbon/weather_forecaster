<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class WeatherbitTest extends TestCase
{
    /**
     * Test if the weatherbit api works successful
     *
     * @return void
     */
    public function testWeatherbitApiSuccess()
    {
        $response = $this->json(
            'POST',
            '/api/weatherbit/forecast',
            [
                'city' => 'Amsterdam',
                'iso2' => 'NL',
            ]
        );

        $response->assertStatus(200);
    }

    /**
     * Test if the weatherbit api validation errors work
     *
     * @return void
     */
    public function testWeatherbitApiValidationError()
    {
        $response = $this->json(
            'POST',
            '/api/weatherbit/forecast',
            [
                'city' => 'Amsterdam',
                'iso2' => 'NLD', // Set wrong size
            ]
        );

        $response->assertStatus(422);
    }

    /**
     * Test the weatherbit api when city not found
     *
     * @return void
     */
    public function testWeatherbitApiEmpty()
    {
        $response = $this->json(
            'POST',
            '/api/weatherbit/forecast',
            [
                'city' => 'IJmuiden',
                'iso2' => 'NL',
            ]
        );

        $response->assertJsonCount(0);
    }
}
