/**
 * Get CSRF token from <meta> tag in the <head>
 *
 * @return {string}
 */
export function getToken() {
    try {
        return document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    } catch (e) {
        console.warn('Failed to read csrf-token from meta tag',
            'https://laravel.com/docs/csrf#csrf-x-csrf-token');
        return '';
    }
}
