import Vue from 'vue'
import {BootstrapVue} from 'bootstrap-vue'
import './modules/api'
import WeatherForecast from './components/WeatherForecast'
import Search from './components/Search'
import Results from './components/Results'

if (process.env.APP_ENV === 'local') {
    Vue.config.devtools = true
}

Vue.use(BootstrapVue)
Vue.use(require('vue-moment'))
Vue.component('WeatherForecast', WeatherForecast)
Vue.component('Search', Search)
Vue.component('Results', Results)

export const app = new Vue({
    el: '#app'
})
