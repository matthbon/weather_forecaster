import {getToken} from '../helpers'
import axios from 'axios'

const token = getToken()

export const weatherApi = axios.create({
    headers: {
        "X-CSRF-TOKEN": token
    }
})
