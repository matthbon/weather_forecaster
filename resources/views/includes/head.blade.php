<meta charset="utf-8">
<meta name="description" content="A tool to forecast the temperature of a City">
<meta name="author" content="Matthijs Bon">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>Weather forecaster</title>

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link href="{{ mix('css/app.css') }}" media="all" rel="stylesheet" type="text/css"/>
