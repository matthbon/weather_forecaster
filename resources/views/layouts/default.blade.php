<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div id="app" class="container">

    <header class="row">
        @include('includes.header')
    </header>

    <div id="main" class="row">
        @yield('content')
    </div>

    <footer class="row">
        @include('includes.footer')
    </footer>

</div>
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
