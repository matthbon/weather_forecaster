<?php

namespace App\Services\Api\Contracts;

use GuzzleHttp\Client;

interface ApiProvider
{
    /**
     * @param Client $client
     */
    public function setClient(Client $client);

    /**
     * @return Client
     */
    public function getClient(): Client;

    /**
     * @param $settings
     */
    public function setSettings(array $settings);

    /**
     * @return array
     */
    public function getSettings(): array;

    /**
     * @param $request
     */
    public function setRequest(string $request);

    /**
     * @return string
     */
    public function getRequest(): string;

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers = []);

    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @param int $timeout
     */
    public function setTimeout(int $timeout);

    /**
     * @return int
     */
    public function getTimeout(): int;

    /**
     * @param bool $jsonDecode
     */
    public function setJsonDecode(bool $jsonDecode);

    /**
     * @return bool
     */
    public function getJsonDecode(): bool;

    /**
     * Do a get request to an url
     */
    public function get();

    /**
     * Do a post request to an url with given input array
     *
     * @param $data
     */
    public function post(array $data);

    /**
     * Do a delete request to an url with given input array
     */
    public function delete();
}
