<?php

namespace App\Services\Api;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use App\Services\Api\Contracts\ApiProvider;
use RuntimeException;

abstract class ApiService implements ApiProvider
{
    /**
     * @var Client
     */
    protected $client;
    /**
     * @var array
     */
    protected $settings;
    /**
     * @var string
     */
    protected $request;
    /**
     * @var array
     */
    protected $headers = [];
    /**
     * @var int
     */
    protected $timeout = 2;
    /**
     * @var bool
     */
    protected $jsonDecode = true;

    /**
     * @param Client $client
     *
     * @return void
     */
    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param $settings
     *
     * @return void
     */
    public function setSettings(array $settings): void
    {
        $this->settings = $settings;
    }

    /**
     * @return array
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * @param string $request
     *
     * @return void
     */
    public function setRequest(string $request): void
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getRequest(): string
    {
        return $this->request;
    }

    /**
     * @param array $headers
     *
     * @return void
     */
    public function setHeaders(array $headers = []): void
    {
        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param int $timeout
     *
     * @return void
     */
    public function setTimeout(int $timeout): void
    {
        $this->timeout = $timeout;
    }

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @param bool $jsonDecode
     *
     * @return void
     */
    public function setJsonDecode(bool $jsonDecode): void
    {
        $this->jsonDecode = $jsonDecode;
    }

    /**
     * @return bool
     */
    public function getJsonDecode(): bool
    {
        return $this->jsonDecode;
    }

    /**
     * Api constructor.
     *
     * @param Client $client
     * @param array $settings
     */
    public function __construct(Client $client, array $settings)
    {
        $this->setClient($client);
        $this->setSettings($settings);
    }

    /**
     * Do a get request to an url
     *
     * @return mixed|null
     * @throws Exception
     */
    public function get()
    {
        try {
            $response = $this->client->get(
                $this->getRequest(),
                $this->defaultParameters()
            );
        } catch (ConnectException $e) {
            $response = null;
            throw new RuntimeException(
                __('Connection error') . PHP_EOL . $e->getResponse()->getBody()->getContents(),
                503,
                null
            );
        } catch (Exception $e) {
            $response = null;
            throw new RuntimeException(
                __('Invalid request') . PHP_EOL . $e->getMessage(),
                503,
                null
            );
        }

        if (null === $response) {
            return [];
        }

        return $this->getOutput($response);
    }

    /**
     * Do a post request to an url with given input array
     *
     * @param array $data
     *
     * @return mixed|null
     * @throws Exception
     */
    public function post(array $data)
    {
        try {
            $post = array_merge(
                [
                    'json' => $data
                ],
                $this->defaultParameters()
            );

            $response = $this->client->post(
                $this->getRequest(),
                $post
            );
        } catch (ConnectException $e) {
            $response = null;
            throw new RuntimeException(
                __('Connection error') . PHP_EOL . $e->getResponse()->getBody()->getContents(),
                503,
                null
            );
        } catch (Exception $e) {
            $response = null;
            throw new RuntimeException(
                __('Invalid request') . PHP_EOL . $e->getMessage(),
                503,
                null
            );
        }

        if (null === $response) {
            return [];
        }

        return $this->getOutput($response);
    }

    /**
     * Do a delete request to an url with given input array
     *
     * @return mixed|null
     * @throws Exception
     */
    public function delete()
    {
        try {
            $response = $this->client->delete(
                $this->getRequest(),
                $this->defaultParameters()
            );
        } catch (ConnectException $e) {
            $response = null;
            throw new RuntimeException(
                __('Connection error') . PHP_EOL . $e->getResponse()->getBody()->getContents(),
                503,
                null
            );
        } catch (Exception $e) {
            $response = null;
            throw new RuntimeException(
                __('Invalid request') . PHP_EOL . $e->getMessage(),
                503,
                null
            );
        }

        if (null === $response) {
            return [];
        }

        return $this->getOutput($response);
    }

    /**
     * Return default parameters
     *
     * @return array
     */
    private function defaultParameters(): array
    {
        $parameters = [];
        $parameters['timeout'] = $this->getTimeout();
        $parameters['headers'] = $this->getHeaders();
        return $parameters;
    }

    /**
     * @param $response
     *
     * @return mixed
     * @throws Exception
     */
    private function getOutput($response)
    {
        if ($this->getJsonDecode()) {
            $contents = $response->getBody()->getContents();
            if (empty($contents) || $contents === '') {
                throw new RuntimeException(__('Not found'), 404, null);
            }
            return \json_decode($contents);
        }

        return $response->getBody()->getContents();
    }
}
