<?php

namespace App\Services\Api\Weatherbit\Contracts;

interface WeatherbitInterface
{
    /**
     * @param string $city
     * @param string $iso2
     */
    public function getForecastByCity(string $city, string $iso2);
}
