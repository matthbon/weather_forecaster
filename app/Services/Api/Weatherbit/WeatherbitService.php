<?php

namespace App\Services\Api\Weatherbit;

use App\Services\Api\ApiService;
use App\Services\Api\Weatherbit\Contracts\WeatherbitInterface;
use Exception;
use Illuminate\Support\Facades\Cache;

class WeatherbitService extends ApiService implements WeatherbitInterface
{
    private const DEFAULT_DAYS = 10;
    private const CACHE_TIME_IN_SECONDS = 7200;
    private const DAILY_FORECAST_URI = 'forecast/daily';

    /**
     * @param string $city
     * @param string $iso2
     * @return mixed|null
     * @throws Exception
     */
    public function getForecastByCity(string $city, string $iso2)
    {
        return Cache::remember(
            sprintf('%s_%s', $city, $iso2),
            self::CACHE_TIME_IN_SECONDS,
            function () use ($city, $iso2) {
                $settings = config('api.weatherbit.settings');
                $daysToGet = self::DEFAULT_DAYS;
                $queryString = "?city={$city}&country={$iso2}&days={$daysToGet}&key={$settings['api_key']}";
                $this->setRequest(self::DAILY_FORECAST_URI . $queryString);

                return $this->get();
            }
        );
    }
}
