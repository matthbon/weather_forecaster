<?php

namespace App\Providers;

use App\Services\Api\Weatherbit\Contracts\WeatherbitInterface;
use App\Services\Api\Weatherbit\WeatherbitService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(WeatherbitInterface::class, static function () {
            $config = config('api.weatherbit');
            $client = new Client($config['client']);
            return new WeatherbitService($client, $config['settings']);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
