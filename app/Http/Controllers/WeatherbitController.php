<?php

namespace App\Http\Controllers;

use App\Http\Requests\WeatherbitRequest;
use App\Services\Api\Weatherbit\Contracts\WeatherbitInterface;
use Exception;
use Illuminate\Http\JsonResponse;

class WeatherbitController extends Controller
{
    /**
     * @param WeatherbitRequest $request
     * @param WeatherbitInterface $weatherbitService
     * @return JsonResponse
     */
    public function getForecast(WeatherbitRequest $request, WeatherbitInterface $weatherbitService): JsonResponse
    {
        try {
            $response = $weatherbitService->getForecastByCity(
                $request->post('city'),
                $request->post('iso2')
            );
        } catch (Exception $e) {
            // Do some logging
            $response = [];
        }
        return response()->json($response);
    }
}
